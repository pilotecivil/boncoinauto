#!/usr/bin/env python
# -*- coding: utf-8 -*-

import mechanize
from bs4 import BeautifulSoup
import re
import unicodedata
from unidecode import unidecode

#~  import shelve => sauv et lecture obj python
 

from distanceTime import *


br=mechanize.Browser()
br.set_handle_robots(False)

## recuperation du code html de la page
page=br.open('http://www.leboncoin.fr/velos/offres/rhone_alpes/?f=a&th=1&pe=9')
source=page.read()


soup = BeautifulSoup(source)
liste_annonces=soup.select("[class~=list-lbc]")[0].find_all('a')
print len(liste_annonces)

liste_annonce_parse=[]

ADRESSE_OR='VilleurbanneRhoneFrance'

for annonce in liste_annonces:
	html=BeautifulSoup(str(annonce))
	
	lieu=html.select("[class~=placement]")[0].string.encode('utf-8').strip()	
	lieu=lieu.replace('\xc3\xa2', 'a')
	lieu=lieu.replace('\xc3\xb4', 'o')
	pattern="\s*(.+)\s*\/\s*(.*)\s*"
	match=returnMatching(pattern, lieu)
	if match:
		lieu=str(unidecode(match(1).decode('utf-8'))).replace(" ","")+str(unidecode(match(2).decode('utf-8'))).replace(" ","")+'France'
		print lieu
		
		title=html.select("[class~=title]")[0].string.encode('utf-8').strip()	
		title=str(unidecode(title.decode('utf-8')))
		print title

		if computeDistanceTime(lieu) and checkTitle(title): # si temps correct, on recup le reste + ajout liste
			
			prix=html.select("[class~=price]")[0].string
			pattern="\s*([0-9]+).*"
			match=returnMatching(pattern, prix)
			prix=match(1)

			date=str(html.select("[class~=date]")[0].find_all('div')[1].string)
		
			dico={}
			dico['lien']=html.find_all('a')[0].get('href')
			dico['date']=date
			dico['prix']=int(prix)
			dico['img']=html.select("[class~=image-and-nb]")[0].find_all('img')[0]['src']
			
			liste_annonce_parse.append(dico)

	

print len(liste_annonce_parse)
	

